# Q1ASM 

This VS Code extension provides support for the Q1 Assembly Language (Q1ASM) used in equipment produced by [Qblox](http://www.qblox.com). For more information about the Qblox products and the assembly language itself, see the [Qblox Instruments Read the Docs](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/)

This extension provides support for:

* Q1 Assembly syntax highlighting.

## Screenshots

Syntax highlighting:

![SyntaxHighlight](images/Screenshot_Syntax_Highlight.png)


## License

The source code to this extension is available on gitlab and licensed under the [BSD 4-Clause license](LICENSE).