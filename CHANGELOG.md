# Change Log

All notable changes to the "q1asm" extension will be documented in this file.

## 0.1.0 - 03.02.2022

* Initial release
* Syntax highlighting
